Repositórios oficiais do programa "GParted"

https://gitlab.com/antiX-Linux/cli-aptiX


Traduções revisadas por marcelocripe:

https://gitlab.com/marcelocripe/cli-aptiX_pt_BR/-/blob/main/cli-aptiX_pt_BR_01-04-2024.po?ref_type=heads

https://gitlab.com/marcelocripe/cli-aptiX_pt_BR/-/blob/main/cli-aptiX.desktop?ref_type=heads



Para utilizar o arquivo "cli-aptiX_pt_BR_01-04-2024.po" e o "cli-aptiX.desktop", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.

Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt cli-aptiX_pt_BR_01-04-2024.po -o cli-aptiX.mo


Comando para renomear o arquivo antigo da tradução com a extensão ".mo" que está na pasta do idioma "pt_BR".

$ sudo mv /usr/share/locale/pt_BR/LC_MESSAGES/cli-aptiX.mo /usr/share/locale/pt_BR/LC_MESSAGES/cli-aptiX_antigo.mo


Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp cli-aptiX.mo /usr/share/locale/pt_BR/LC_MESSAGES


Comando para copiar o arquivo com a extensão ".desktop" para a pasta /usr/share/applications.

$ sudo cp cli-aptiX.desktop /usr/share/applications


Comando para escrever globalmente todas as entradas dos menus do antiX:

$ sudo desktop-menu --write-out-global